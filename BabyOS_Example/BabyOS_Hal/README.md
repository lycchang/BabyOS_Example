# BabyOS

## bos_hal

![logo](https://gitee.com/notrynohigh/BabyOS/raw/master/doc/2.png)





```C
.
├── b_hal.c
├── b_hal_gpio.c
├── b_hal_i2c.c
├── b_hal_lcd.c
├── b_hal_qspi.c
├── b_hal_sccb.c
├── b_hal_spi.c
├── b_hal_uart.c
└── inc
    ├── b_hal_gpio.h
    ├── b_hal.h
    ├── b_hal_i2c.h
    ├── b_hal_lcd.h
    ├── b_hal_qspi.h
    ├── b_hal_sccb.h
    ├── b_hal_spi.h
    └── b_hal_uart.h

```



**不同分支对应不同的硬件平台，希望使用了BabyOS的朋友，将hal部分的代码开源分享！**



管理员邮箱：notrynohigh@outlook.com

开发小组群：

![qq](https://gitee.com/notrynohigh/BabyOS/raw/master/doc/qq.png)



